﻿
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017-2018 Christian 'ketura' McCarty                             ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;

using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Markup.Xaml;

namespace BillsPC
{
	public static class ControlExtensions
	{
		public static Control FindInChildren(this ILogical log, string name)
		{
			var children = log.LogicalChildren;

			foreach (var child in children)
			{
				if ((child as Control).Name == name)
				{
					return child as Control;
				}
				else
				{
					var depth = child.FindInChildren(name);
					if(depth != null)
					{
						return depth;
					}
				}
			}

			return null;
		}

		public static T FindInChildren<T>(this ILogical log, string name)
			where T : Control
		{
			return log.FindInChildren(name) as T;
		}
	}
}
