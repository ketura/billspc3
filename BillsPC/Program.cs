﻿using System;
using Avalonia;
using Avalonia.Logging.Serilog;
using BillsPC.ViewModels;
using BillsPC.Views;

namespace BillsPC
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildAvaloniaApp().Start<MainWindow>(() => new MainWindowViewModel());
        }

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .UseReactiveUI()
                .LogToDebug();
    }
}
