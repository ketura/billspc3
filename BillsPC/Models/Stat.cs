﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillsPC.Models
{
    public class Stat : ReactiveObject
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { this.RaiseAndSetIfChanged(ref _name, value); }
        }

        private string _abbr;
        public string Abbr
        {
            get { return _abbr; }
            set { this.RaiseAndSetIfChanged(ref _abbr, value); }
        }

        private int _defaultValue;
        public int DefaultValue
        {
            get { return _defaultValue; }
            set { this.RaiseAndSetIfChanged(ref _defaultValue, value); }
        }

        private int _defaultSpread;
        public int DefaultSpread
        {
            get { return _defaultSpread; }
            set { this.RaiseAndSetIfChanged(ref _defaultSpread, value); }
        }
    }
}
