﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillsPC.Models
{
    public class DefinitionLoader
    {
        public static List<Stat> GetStats()
        {
            return new List<Stat>()
            {
                new Stat(){ Name = "Hit Points", Abbr = "HP", DefaultValue = 100, DefaultSpread = 20 },
                new Stat(){ Name = "Attack", Abbr = "ATK", DefaultValue = 100, DefaultSpread = 20 },
                new Stat(){ Name = "Defense", Abbr = "DEF", DefaultValue = 100, DefaultSpread = 20 },
                new Stat(){ Name = "Movement", Abbr = "MOV", DefaultValue = 100, DefaultSpread = 20 },
                new Stat(){ Name = "Endurance", Abbr = "END", DefaultValue = 100, DefaultSpread = 20 }

            };

        }
    }
}
