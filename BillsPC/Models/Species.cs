﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillsPC.Models
{
    public class Species
    {
        public string Name { get; set; }
        public int ID { get; set; }

        public int HP { get; set; }
        public int HPSpread { get; set; }

        public int Attack { get; set; }
        public int AttackSpread { get; set; }

        public int Defense { get; set; }
        public int DefenseSpread { get; set; }

        public int Movement { get; set; }
        public int MovementSpread { get; set; }

        public int Endurance { get; set; }
        public int EnduranceSpread { get; set; }

    }
}
