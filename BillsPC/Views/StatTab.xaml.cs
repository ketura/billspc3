﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace BillsPC.Views
{
    public class StatTab : UserControl
    {
        public StatTab()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
