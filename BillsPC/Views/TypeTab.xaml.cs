﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace BillsPC.Views
{
    public class TypeTab : UserControl
    {
        public TypeTab()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
