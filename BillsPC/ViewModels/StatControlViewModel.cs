﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BillsPC.ViewModels
{
    public class StatControlViewModel : ViewModelBase
    {
        private string _greeting;
        public string Greeting
        {
            get { return _greeting; }
            set { this.RaiseAndSetIfChanged(ref _greeting, value); }
        }

        private string selectedVersion;
        public string SelectedVersion
        {
            get
            {
                return selectedVersion;
            }
            set
            {
                string ret = this.RaiseAndSetIfChanged(ref selectedVersion, value);
                Greeting = selectedVersion;
            }
        }

        private ObservableCollection<string> _versions;

        public ObservableCollection<string> Versions
        {
            get { return _versions; }
            set { this.RaiseAndSetIfChanged(ref _versions, value); }
        }

        public StatControlViewModel()
        {
            Greeting = "Hello, World!";
            Versions = new ObservableCollection<string>() { "Option 1", "Option B", "Third Option" };
        }
    }
}
