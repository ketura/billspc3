﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BillsPC.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string _greeting;
        public string Greeting
        {
            get { return _greeting; }
            set { this.RaiseAndSetIfChanged(ref _greeting, value); }
        }

        private string selectedVersion;
        public string SelectedVersion
        {
            get
            {
                return selectedVersion;
            }
            set
            {
                string ret = this.RaiseAndSetIfChanged(ref selectedVersion, value);
                Greeting = selectedVersion;
            }
        }

        private StatTabViewModel _statTabVM;
        public StatTabViewModel StatTabVM
        {
            get
            {
                return _statTabVM;
            }
            set
            {
                StatTabViewModel ret = this.RaiseAndSetIfChanged(ref _statTabVM, value);
            }
        }



        private ObservableCollection<string> _versions;

        public ObservableCollection<string> Versions
        {
            get { return _versions; }
            set { this.RaiseAndSetIfChanged(ref _versions, value); }
        }

        public MainWindowViewModel()
        {
            Greeting = "Hello, World!";
            Versions = new ObservableCollection<string>() { "Option 1", "Option B", "Third Option" };
            StatTabVM = new StatTabViewModel();
        }
    }
}
