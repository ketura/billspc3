﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using BillsPC.Models;

namespace BillsPC.ViewModels
{
    public class StatTabViewModel : ViewModelBase
    {
        private string _greeting;
        public string Greeting
        {
            get { return _greeting; }
            set { this.RaiseAndSetIfChanged(ref _greeting, value); }
        }

        private Stat _selectedStat;
        public Stat SelectedStat
        {
            get
            {
                return _selectedStat;
            }
            set
            {
                Stat ret = this.RaiseAndSetIfChanged(ref _selectedStat, value);
                Greeting = _selectedStat?.Abbr;
            }
        }

        private ObservableCollection<Stat> _stats;

        public ObservableCollection<Stat> Stats
        {
            get { return _stats; }
            set { this.RaiseAndSetIfChanged(ref _stats, value); }
        }

        public StatTabViewModel()
        {
            Greeting = "Hello, World!";
            Stats = new ObservableCollection<Stat>(DefinitionLoader.GetStats());
        }
    }
}
